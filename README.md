## Start

Download this repository and install it in your server directory.

A dump of the database structure is located at /core/database_dump/ , please use it.

---

## Config

To configure the connection to the database server, edit the file /core/Config.php
