<div class="error">
    <div class="error-code m-b-10 m-t-20">404 <i class="fa fa-warning"></i></div>
    <h3 class="font-bold"><?= $t_title ?></h3>

    <div class="error-desc">
        <?= $t_description ?>
        <div>
            <a class="login-detail-panel-button btn" href="/<?= $this->lang ?>" >
                <i class="fa fa-arrow-left"></i>
                <?= $t_back ?>
            </a>
        </div>
    </div>
</div>