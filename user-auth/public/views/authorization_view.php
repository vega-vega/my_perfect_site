<?php
if (isset($t_err)) {
?>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="alert alert-danger" role="alert">
            <?= $t_err ?>
        </div>
    </div>
</div>
<?php
}
?>
<div class="row">
    <div class= "col-md-offset-4 col-md-4">
        <form role="form" method="post">
            <fieldset>
                <p class="text-uppercase"><?= $t_title ?></p>

                <div class="form-group">
                    <input type="email" name="authorization[login]" id="login" class="form-control input-lg" placeholder="Email">
                </div>
                <div class="form-group">
                    <input type="password" name="authorization[password]" id="password" class="form-control input-lg" placeholder="Password">
                </div>
                <div>
                    <input type="submit" class="btn btn-md" value="<?= $t_login ?>">
                    <a class="login-detail-panel-button btn pull-right" href="/registration/<?= $this->lang ?>" >
                        <?= $t_register ?>
                    </a>
                </div>
            </fieldset>
        </form>
    </div>
</div>