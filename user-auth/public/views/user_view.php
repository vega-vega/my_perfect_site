<div class="row">
    <div class="col-md-offset-3 col-md-5">
        <img data-src="holder.js/200x200" class="img-thumbnail" alt="200x200" style="width: 200px; height: 200px;" src="data:image/<?= $image_type ?>;base64,<?= $image ?>" data-holder-rendered="true">
    </div>
</div>
<br>
<div class="row">
    <div class="col-md-offset-3 col-md-5">
        <b><?= $t_name ?></b>
        <p><?= $name ?></p>
    </div>
</div>
<div class="row">
    <div class="col-md-offset-3 col-md-5">
        <b><?= $t_email ?></b>
        <p><?= $email ?></p>
    </div>
</div>
<div class="row">
    <div class="col-md-offset-3 col-md-5">
        <b><?= $t_city ?></b>
        <p><?= $city ?></p>
    </div>
</div>
<div class="row">
    <div class="col-md-offset-3 col-md-5">
        <b><?= $t_description ?></b>
        <p><?= $description ?></p>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-offset-2 col-md-4">
        <p><a class="btn btn-default" href="/edit/<?= $this->lang ?>" role="button"><?= $t_edit ?></a></p>
    </div>
    <div class="col-md-4">
        <p><a class="btn btn-default" href="/user/exit/<?= $this->lang ?>" role="button"><?= $t_exit ?></a></p>
    </div>
</div>