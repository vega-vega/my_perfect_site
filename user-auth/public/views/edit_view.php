<?php
$is_err = array_key_exists('err', $data);
if ($is_err && array_count_values($data['err']) > 0) {
    ?>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="alert alert-danger" role="alert">
                <?php
                foreach ($data['err'] as $err) {
                    if (is_string($err)) {
                        echo '<li>' . $err . '</li>';
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <?php
}
?>

<div class="row">
    <div class="col-md-offset-3 col-md-5">
        <form role="form" method="post" enctype="multipart/form-data" data-toggle="validator">
            <fieldset>
                <p class="text-uppercase text-center pull-center"><?= $t_title ?></p>
                <div class="col-md-12">
                    <div class="form-group text-center">
                        <img data-src="holder.js/200x200" class="img-thumbnail" alt="200x200" style="width: 200px; height: 200px;" src="data:image/<?= $image_type ?>;base64,<?= $image ?>" data-holder-rendered="true">
                    </div>
                </div>
                <div class="col-md-offset-3 col-md-9">
                    <div class="form-group text-center">
                        <input type="file" name="image">
                    </div>
                </div>
                <div class="form-group <?= $is_err && isset($data['err']) ? 'has-error' : ''?>">
                    <input type="text" data-error="<?= $t_err_name ?>" pattern="^[_A-zА-я ]{1,}$" name="edit[name]" id="name" class="form-control input-lg" placeholder="Name" value="<?= $name ?>" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input type="text" name="edit[city]" id="city" class="form-control input-lg" placeholder="City" value="<?= $city ?>">
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="edit[description]" id="description" rows="5" placeholder="About me"><?= $description ?></textarea>
                </div>
                <div>
                    <div class="col-md-6 text-left">
                        <input type="submit" class="btn btn-lg btn-primary" value="<?= $t_update ?>">
                    </div>
                    <div class="col-md-6 text-right">
                        <a type="submit" class="btn btn-lg btn-primary" href="/user/<?= $this->lang ?>">
                            <?= $t_back ?>
                        </a>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
