<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/public/css/bootstrap.min.css" id="bootstrap-css">
    <?php
        if (isset($styles)) {
            foreach ($styles as $style) {
                echo '<link rel="stylesheet" href="/public/css/' . $style . '.css" id="bootstrap-css">';
            }
        }
    ?>
    <script src="/public/js/jquery.min.js"></script>
    <script src="/public/js/bootstrap.min.js"></script>
    <?php
        if (isset($scripts)) {
            foreach ($scripts as $script) {
                echo '<script src="/public/js/' . $script . '"></script>';
            }
        }
    ?>
    <title>My perfect site</title>
</head>
<body>
    <div class="container">
        <?php include 'public/views/template_header_view.php'; ?>
        <?php include 'public/views/' . $content_view; ?>
    </div>
</body>
</html>