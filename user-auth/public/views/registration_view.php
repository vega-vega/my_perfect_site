<?php
$is_err = array_key_exists('err', $data);
if ($is_err && array_count_values($data['err']) > 0) {
?>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="alert alert-danger" role="alert">
<?php
    foreach ($data['err'] as $err) {
        if (is_string($err)) {
            echo '<li>' . $err . '</li>';
        }
    }
?>
            </div>
        </div>
    </div>
<?php
}
?>

<div class="row">
    <div class="col-md-offset-3 col-md-5">
        <form role="form" method="post" id="regForm" data-toggle="validator">
            <fieldset>
                <p class="text-uppercase pull-center"><?= $t_title ?></p>
                <div class="form-group <?= $is_err && isset($data['err']['name']) ? 'has-error' : ''?>">
                    <input type="text" data-error="<?= $t_err_name ?>" pattern="^[_A-zА-я ]{1,}$" name="registration[name]" id="name" class="form-control input-lg" placeholder="Name" value="<?php echo isset($_POST['registration']['name']) ? $_POST['registration']['name'] : '' ?>"  required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group <?= $is_err && isset($data['err']['email']) ? 'has-error' : ''?>">
                    <input type="email" data-error="<?= $t_err_email ?>" name="registration[email]" id="email" class="form-control input-lg" placeholder="Email Address" value="<?php echo isset($_POST['registration']['email']) ? $_POST['registration']['email'] : '' ?>" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group <?= $is_err && isset($data['err']['password']) ? 'has-error' : ''?>">
                    <input type="password" data-error="<?= $t_err_password ?>" data-minlength="6" name="registration[password]" id="password" class="form-control input-lg" placeholder="Password" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group <?= $is_err && isset($data['err']['password']) ? 'has-error' : ''?>">
                    <input type="password" data-error="<?= $t_err_password ?>" data-minlength="6" name="registration[password2]" id="password2" class="form-control input-lg" placeholder="Password2" required>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-check <?= $is_err && isset($data['err']['policy']) ? 'has-error' : ''?>">
                    <label class="form-check-label">
                        <span class="help-block">
                            <input type="checkbox" name="registration[policy]" class="form-check-input" <?php echo isset($_POST['registration']['policy']) ? 'checked' : '' ?>>
                            <?= $t_policy ?>
                        </span>
                    </label>
                </div>
                <div>
                    <input type="submit" class="btn btn-lg btn-primary" value="<?= $t_register ?>">
                </div>
            </fieldset>
        </form>
    </div>
</div>
