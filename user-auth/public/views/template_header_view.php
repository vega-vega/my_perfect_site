<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="/<?= $this->lang ?>">My perfect site</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Lang <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php
                            $url = rtrim(str_replace(Lang_Reader::$LANGS, "", $_SERVER['REQUEST_URI']), "/");
                            foreach (Lang_Reader::$LANGS as $key => $value) {
                                if ($value != $this->lang) {
                                    echo '<li><a href="'. $url . "/" . $value .'">' . $key . '</a></li>';
                                }
                            }
                        ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
