<?php

ini_set('display_errors', 1);
session_start();
require_once 'core/Config.php';
require_once 'core/Lang_Reader.php';
require_once 'core/Model.php';
require_once 'core/Controller.php';
require_once 'core/View.php';
require_once 'core/Route.php';
require_once 'core/Database.php';

Route::start();