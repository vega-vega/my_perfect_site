<?php

class View {

    public $lang;

    public function __construct($lang) {
        $this->lang = $lang;
    }

    public function generate($content_view, $template_view, $data = null, $styles = null, $scripts = null) {
        if(is_array($data)) {
            extract($data);
        }

        include 'public/views/' . $template_view;
    }
}