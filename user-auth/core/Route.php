<?php

class Route {

    static function start() {
        $controller_name = "Authorization";
        $action_name = "index";
        $default_lang = Lang_Reader::$LANGS['English'];
        $lang = $default_lang;

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        Route::CheckLang($routes[1], $lang, $default_lang, $controller_name, $action_name);
        if (!empty($routes[1])) {
            $controller_name = $routes[1];
        }

        Route::CheckLang($routes[2], $lang, $default_lang, $controller_name, $action_name);
        if (!empty($routes[2])) {
            $action_name = $routes[2];
        }

        Route::CheckLang($routes[3], $lang, $default_lang, $controller_name, $action_name);

        $controller_name = 'Controller_' . $controller_name;
        $action_name = 'action_' . $action_name;

        $controller_path = "src/controller/" . $controller_name . ".php";
        if (file_exists($controller_path)) {
            include $controller_path;
        } else {
            Route::ErrorPage404();
        }

        $controller = new $controller_name($lang);
        if (method_exists($controller, $action_name)) {
            $controller->$action_name();
        } else {
            Route::ErrorPage404();
        }
    }

    static function ErrorPage404() {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
    }

    static function GoDefaultLangPage($controller, $action) {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('Location:' . $host . $controller . '/' . $action);
    }

    private static function CheckLang(&$rout, &$lang, $default_lang, $controller_name, $action_name) {
        if (in_array($rout, Lang_Reader::$LANGS)) {
            $lang = $rout;

            if (!is_null($lang) && $lang == $default_lang) {
                Route::GoDefaultLangPage($controller_name, $action_name);
            }

            $rout = null;
        }
    }
}