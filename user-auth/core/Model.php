<?php

require_once "src/manager/Manager_Session.php";

abstract class Model {

    protected $lang;
    protected $data = [];
    abstract public function get_data();
    protected const ERR_JSON = "_err";

    public function __construct($lang) {
        $this->lang = $lang;
    }

    protected function get_user_id() {
        $manager = new Manager_Session();
        return $manager->get_user_id();
    }

    public function get_styles() {
        return null;
    }

    public function get_script() {
        return null;
    }
}