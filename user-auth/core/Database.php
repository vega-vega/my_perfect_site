<?php

class Database {

    static function connect() {
        $mysqli = new mysqli(DATABASE_SERVER, DATABASE_USER, DATABASE_PASSWORD, DATABASE_NAME);

        if (mysqli_connect_errno()) {
            printf("Не удалось подключиться: %s\n", mysqli_connect_error());
            exit();
        }

        return $mysqli;
    }

    static function close($mysqli) {
        $mysqli->close();
    }
}