<?php

const DATABASE_SERVER = 'localhost'; // database server.
const DATABASE_USER = 'admin'; // database login user.
const DATABASE_PASSWORD = 'password'; // database login user password.
const DATABASE_NAME = 'my_perfect_site'; // database name.
const SESSION_LIFE_TIME = 10000; // web user session life.