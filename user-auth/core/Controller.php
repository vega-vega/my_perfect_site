<?php

require_once "src/manager/Manager_Session.php";

abstract class Controller {

    public $view;

    function __construct($lang) {
        $this->view = new View($lang);
    }

    abstract function action_index();

    protected function not_authorized() {
        $manager = new Manager_Session();
        if ($manager->check_session()) {
            $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
            header('Location:' . $host . 'user/' . $this->view->lang);
        }
    }

    protected function session_is_ok() {
        $manager = new Manager_Session();
        if (!$manager->check_session()) {
            $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
            header('Location:' . $host . $this->view->lang);
        }
    }

    protected function session_logout() {
        $manager = new Manager_Session();
        $manager->kill_session();
    }
}