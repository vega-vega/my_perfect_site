<?php

class Lang_Reader {

    public static $LANGS = ['English' => 'en', 'Русский' => 'ru'];

    public static function readJson($page_name, $lang) {
        $path = "public/lang/" . $lang . "/" . $page_name . ".json";

        if (file_exists($path)) {
            $json = file_get_contents($path);
        } else {
            $path = "public/lang/eng/" . $page_name . ".json";
            $json = file_get_contents($path);
        }

        return json_decode($json, true);
    }
}