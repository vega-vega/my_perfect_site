<?php

require_once "core/Database.php";

class Manager_User {

    /**
     * Download current user data.
     *
     * @return array
     */
    public function load_user_data($id) {
        $mysqli = Database::connect();
        $data = [];

        if ($result = $mysqli->query("SELECT name, email, city, description, image, image_type FROM user WHERE id = '$id';", MYSQLI_USE_RESULT)) {
            $row = $result->fetch_row();

            $data['name'] = $row[0];
            $data['email'] = $row[1];
            $data['city'] = $row[2];
            $data['description'] = $row[3];
            $data['image'] = $this->get_image($row[4]);
            $data['image_type'] = $this->get_image_type($row[5]);

            $result->free_result();
        }

        Database::close($mysqli);

        return $data;
    }

    private function get_image($image) {
        if (is_null($image)) {
            return "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iMCAwIDIwMCAyMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzIwMHgyMDAKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNzBiOWFlOTk2MSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE3MGI5YWU5OTYxIj48cmVjdCB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgZmlsbD0iI0VFRUVFRSIvPjxnPjx0ZXh0IHg9Ijc0LjA0Njg3NSIgeT0iMTA0LjUiPjIwMHgyMDA8L3RleHQ+PC9nPjwvZz48L3N2Zz4=";
        }

        return base64_encode($image);
    }

    private function get_image_type($image_type) {
        if (is_null($image_type)) {
            return "svg+xml";
        }

        return $image_type;
    }
}