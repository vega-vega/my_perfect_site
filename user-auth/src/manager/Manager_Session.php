<?php

require_once "core/Database.php";

class Manager_Session {

    /**
     * Create authorization user session.
     */
    public function create_session($user_id) {
        $mysqli = Database::connect();

        $hash = bin2hex(random_bytes(15));
        $mysqli->query("INSERT into session (user_id, hash, date) VALUES ('$user_id', '$hash', NOW()) ON DUPLICATE KEY UPDATE hash = VALUES(hash), date = VALUES(date);");

        Database::close($mysqli);

        $_SESSION['session_hash'] = $hash;
    }

    /**
     * Check user session is alive.
     * @return bool
     */
    public function check_session() {
        $is_valid = false;
        if (array_key_exists('session_hash', $_SESSION)) {
            $hash = $_SESSION['session_hash'];

            $mysqli = Database::connect();
            if ($result = $mysqli->query("SELECT id, date FROM session WHERE hash = '$hash';", MYSQLI_USE_RESULT)) {
                $row = $result->fetch_row();
                $id = $row[0];
                $data = $row[1];
                $result->free_result();

                if ((strtotime(date("Y-m-d H:i:s")) - strtotime($data)) > SESSION_LIFE_TIME) {
                    $mysqli->query("DELETE FROM session WHERE id = '$id';");
                    unset($_SESSION['session_hash']);
                } else {
                    $this->update_session_life($mysqli, $id);
                    $is_valid = true;
                }
            }

            Database::close($mysqli);
        }

        return $is_valid;
    }

    /**
     * Get user id by current session.
     */
    public function get_user_id() {
        $id = null;
        if (array_key_exists('session_hash', $_SESSION)) {
            $hash = $_SESSION['session_hash'];
            $mysqli = Database::connect();
            if ($result = $mysqli->query("SELECT user_id FROM session WHERE hash = '$hash';", MYSQLI_USE_RESULT)) {
                $row = $result->fetch_row();
                $id = $row[0];
                $result->free_result();
            }

            Database::close($mysqli);
        }

        return $id;
    }

    /**
     * Destroy current session;
     */
    public function kill_session() {
        if (array_key_exists('session_hash', $_SESSION)) {
            $hash = $_SESSION['session_hash'];
            $mysqli = Database::connect();
            if ($mysqli->query("DELETE FROM session WHERE hash = '$hash';")) {
                unset($_SESSION['session_hash']);
            }

            Database::close($mysqli);
        }
    }

    private function update_session_life($mysqli, $session_id) {
        $mysqli->query("UPDATE session SET date = NOW() WHERE id = '$session_id';");
    }
}