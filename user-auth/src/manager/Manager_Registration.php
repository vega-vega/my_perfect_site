<?php

class Manager_Registration {

    /**
     * Registration new user in database.
     * If ok, return true.
     * If email exist, return false.
     * Otherwise return string.
     *
     * @return bool|string
     */
     public function registration($values) {
         $mysqli = Database::connect();

         $name = $mysqli->real_escape_string($values['name']);
         $email = $mysqli->real_escape_string($values['email']);
         $password = password_hash($values['password'], PASSWORD_DEFAULT);

         $err = true;
         if (!$mysqli->query("INSERT into user (name, email, password) VALUES ('$name', '$email', '$password')")) {
             if (23000 == $mysqli->sqlstate) {
                 $err = false;
             } else {
                 $err = $mysqli->error;
             }
         }

         Database::close($mysqli);

         return $err;
     }
}