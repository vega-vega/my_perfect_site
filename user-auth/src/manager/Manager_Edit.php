<?php

require_once "core/Database.php";

class Manager_Edit {

    const EXTENSIONS = ['jpeg','jpg','png','gif'];
    const LANG_PATH = "edit_err";
    private $data = [];
    private $err_data;

    /**
     * Update user data in database if valid.
     */
    public function update_user_data($lang) {
        if (!isset($_POST['edit'])) {
            return;
        }

        $manager = new Manager_Session();
        $user_id = $manager->get_user_id();
        $this->err_data = Lang_Reader::readJson(self::LANG_PATH, $lang);

        $this->update_name($user_id, $_POST['edit']['name']);
        $this->update_city($user_id, $_POST['edit']['city']);
        $this->update_description($user_id, $_POST['edit']['description']);
        $this->update_image($user_id, $_FILES['image']);
    }

    public function get_data() {
        return $this->data;
    }

    private function update_name($user_id, $name) {
        if (!preg_match("/^[A-zА-я ]+$/iu", $name)) {
            $this->data['err'] = [$this->err_data["t_err_name"]];
            return;
        }

        $mysqli = Database::connect();
        $name = $mysqli->real_escape_string($name);
        $mysqli->query("UPDATE user SET name = '$name' WHERE id = '$user_id'");
        Database::close($mysqli);
    }

    private function update_city($user_id, $city) {
        $mysqli = Database::connect();
        $city = $mysqli->real_escape_string($city);
        $mysqli->query("UPDATE user SET city = '$city' WHERE id = '$user_id'");
        Database::close($mysqli);
    }

    private function update_description($user_id, $description) {
        $mysqli = Database::connect();
        $description = $mysqli->real_escape_string($description);
        $mysqli->query("UPDATE user SET description = '$description' WHERE id = '$user_id'");
        Database::close($mysqli);
    }

    private function update_image($user_id, $image) {
        if ($image['size'] > 0) {
            $arr = explode('.', $image['name']);
            $image_type = strtolower(end($arr));
            $image_data = addslashes(file_get_contents($image['tmp_name']));

            if (!in_array($image_type, self::EXTENSIONS) == false) {
                $err_data['image'] = "Extension not allowed, please choose a JPEG or PNG or gif file.";
            }
            $mysqli = Database::connect();
            $mysqli->query("UPDATE user SET image = '$image_data', image_type = '$image_type' WHERE id = '$user_id';");
            Database::close($mysqli);
        }
    }
}