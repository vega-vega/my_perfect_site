<?php

require_once 'core/Database.php';
require_once 'src/manager/Manager_Session.php';

class Manager_Authorization {

    /**
     * Check user is valid.
     * @return bool
     */
    public function authorization($login, $password) {
        $is_valid = false;
        $mysqli = Database::connect();

        $login = $mysqli->real_escape_string($login);
        $user_id = null;
        if ($result = $mysqli->query("SELECT id, password FROM user WHERE email = '$login'", MYSQLI_USE_RESULT)) {
            $row = $result->fetch_row();

            $user_id = $row[0];
            $is_valid = password_verify($password, $row[1]);
        }

        Database::close($mysqli);

        if ($is_valid) {
            $manager = new Manager_Session();
            $manager->create_session($user_id);
        }

        return $is_valid;
    }
}