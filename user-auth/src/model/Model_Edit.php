<?php

require_once "src/manager/Manager_User.php";

class Model_Edit extends Model {

    private const LANG_PATH = 'edit';

    public function get_data() {
        $manager = new Manager_User();
        $this->data = $manager->load_user_data($this->get_user_id());

        $this->data = array_merge($this->data, Lang_Reader::readJson(self::LANG_PATH, $this->lang));

        return $this->data;
    }

    public function get_script() {
        return ['validator.min.js'];
    }
}