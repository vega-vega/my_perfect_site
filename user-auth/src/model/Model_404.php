<?php

class Model_404 extends Model {

    public function get_data() {
        return Lang_Reader::readJson("404", $this->lang);
    }

    public function get_styles() {
        return ['404'];
    }
}