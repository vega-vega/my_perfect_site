<?php

require_once 'src/manager/Manager_Authorization.php';

class Model_Authorization extends Model {

    private const LANG_PATH = 'authorization';

    public function get_data() {
        $this->data = array_merge($this->data, Lang_Reader::readJson(self::LANG_PATH, $this->lang));
        return $this->data;
    }

    public function get_styles() {
        return ['form'];
    }

    /**
     * Check user authorization is valid.
     */
    public function check_authorization_form() {
        if (!isset($_POST['authorization'])) {
            return false;
        }

        $values = $_POST['authorization'];
        $manager = new Manager_Authorization();
        $is_valid = $manager->authorization($values['login'], $values['password']);

        if (!$is_valid) {
            $this->data = array_merge($this->data, Lang_Reader::readJson(self::LANG_PATH . self::ERR_JSON, $this->lang));
        }

        return $is_valid;
    }

}
