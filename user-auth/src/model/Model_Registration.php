<?php

require_once "src/manager/Manager_Registration.php";

class Model_Registration extends Model {

    private $err_data;
    private const LANG_PATH = 'registration';

    public function get_data() {
        $this->data = array_merge($this->data, Lang_Reader::readJson(self::LANG_PATH, $this->lang));
        return $this->data;
    }

    public function get_script() {
        return ['validator.min.js'];
    }


    /**
     * User input data for registration is valid.
     */
    public function check_registration_form() {
        if (!isset($_POST['registration'])) {
            return false;
        }

        $this->err_data = Lang_Reader::readJson(self::LANG_PATH . self::ERR_JSON, $this->lang);
        $this->data['err'] = [];
        $values = $_POST['registration'];
        if ($this->check_name($values['name']) & $this->check_email($values['email']) & $this->check_password($values['password'], $values['password2']) & $this->check_policy($values)) {
            $manager = new Manager_Registration();
            $err = $manager->registration($values);

            if ($err) {
                return false;
            }

            $this->data['err']['sql'] = $err == false ? $this->err_data['t_err_email_exist'] : $err;
        }

        return false;
    }

    /**
     * Check user registration name is valid.
     */
    private function check_name($name) {
        if (preg_match("/^[A-zА-я ]+$/iu", $name)) {
            return true;
        }

        $this->data['err']['name'] = $this->err_data['t_err_name'];
        return false;
    }

    /**
     * Check user registration email.
     */
    private function check_email($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }

        $this->data['err']['email'] = $this->err_data['t_err_email'];
        return false;
    }

    /**
     * Check user registration password.
     */
    private function check_password($password, $password2) {
        $is_valid = true;
        if ($password != $password2) {
            $this->data['err']['password'] = true;
            array_push($this->data['err'], $this->err_data['t_err_password_match']);
            $is_valid = false;
        }

        if (strlen($password) < 6) {
            $this->data['err']['password'] = true;
            array_push($this->data['err'], $this->err_data['t_err_password_length']);
            $is_valid = false;
        }

        if (!preg_match('@[A-Z]@', $password)) {
            $this->data['err']['password'] = true;
            array_push($this->data['err'], $this->err_data['t_err_password_upper']);
            $is_valid = false;
        }

        if (!preg_match('@[a-z]@', $password)) {
            $this->data['err']['password'] = true;
            array_push($this->data['err'], $this->err_data['t_err_password_lower']);
            $is_valid = false;
        }

        if (!preg_match('@[0-9]@', $password)) {
            $this->data['err']['password'] = 1;
            array_push($this->data['err'], $this->err_data['t_err_password_number']);
            $is_valid = false;
        }

        return $is_valid;
    }

    private function check_policy($values) {
        $policy = array_key_exists('policy', $values);
        if ($policy) {
            return true;
        }

        $this->data['err']['policy'] = $this->err_data['t_err_policy'];
        return false;
    }
}