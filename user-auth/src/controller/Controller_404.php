<?php

require_once "src/model/Model_404.php";

class Controller_404 extends Controller {

    function action_index() {
        $model = new Model_404($this->view->lang);
        $this->view->generate('404_view.php', 'template_view.php', $model->get_data(), $model->get_styles());
    }

}