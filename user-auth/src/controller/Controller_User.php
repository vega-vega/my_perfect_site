<?php

require_once "src/model/Model_User.php";

class Controller_User extends Controller {

    function action_index() {
        $this->session_is_ok();

        $model = new Model_User($this->view->lang);
        $this->view->generate('user_view.php', 'template_view.php', $model->get_data());
    }

    function action_exit() {
        $this->session_logout();
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('Location:' . $host . $this->view->lang);
    }
}