<?php

require_once "src/model/Model_Registration.php";

class Controller_Registration extends Controller {

    function action_index() {
        $this->not_authorized();

        $model = new Model_Registration($this->view->lang);
        if ($model->check_registration_form()) {
            $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
            header('Location:' . $host . '/authorization/' . $this->view->lang);
        }

        $this->view->generate('registration_view.php', 'template_view.php', $model->get_data(), $model->get_styles(), $model->get_script());
    }

}
