<?php

require_once "src/model/Model_Edit.php";
require_once "src/manager/Manager_Edit.php";

class Controller_Edit extends Controller {

    function action_index() {
        $this->session_is_ok();

        $manager = new Manager_Edit();
        $manager->update_user_data($this->view->lang);

        $model = new Model_Edit($this->view->lang);
        $data = array_merge($model->get_data(), $manager->get_data());
        $this->view->generate('edit_view.php', 'template_view.php', $data, $model->get_styles(), $model->get_script());
    }

}