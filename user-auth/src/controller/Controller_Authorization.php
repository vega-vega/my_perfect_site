<?php

require_once "src/model/Model_Authorization.php";

class Controller_Authorization extends Controller {

    function action_index() {
        $this->not_authorized();
        $model = new Model_Authorization($this->view->lang);

        if ($model->check_authorization_form()) {
            $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
            header('Location:' . $host . 'user/' . $this->view->lang);
        }

        $this->view->generate('authorization_view.php', 'template_view.php', $model->get_data(), $model->get_styles());
    }

}